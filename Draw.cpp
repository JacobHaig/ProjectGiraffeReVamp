#include "draw.h"

using namespace std;

namespace Draw {
	void init() {
		window = new sf::RenderWindow(sf::VideoMode(WIDTH, HEIGHT), "GAME WINDOW!");
		background.setSize(sf::Vector2f(WIDTH, HEIGHT));
		background.setFillColor(sf::Color(10, 200, 0));
	}

	void Draw::drawLogic() {
		if (window->isOpen()) {
			sf::Event event;
			while (window->pollEvent(event))
				if (event.type == sf::Event::Closed)
					window->close();

			window->clear();
			window->draw(background);
			window->display();
		}
	}

	void drawEnemies() {
	}

	void drawMaps(int selection) {
	}

	void drawEntities(int entSelected, int mapSelection) {
	}
}