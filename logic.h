#pragma once
#include <string>

#include "person.h"
#include "draw.h"
#include "input.h"
#include "SFML/Graphics.hpp"

using namespace std;

union GameLogic {
public:
	bool GameOver = false;
	Person player;

	GameLogic();
	~GameLogic();
	void loop();
};
