#include "input.h"
#include "SFML/Graphics.hpp"
#include <iostream>

#define key sf::Keyboard
#define pressed sf::Keyboard::isKeyPressed

namespace Input {
	void getInput() {
	}

	sf::Vector2f getInputDirection() {
		sf::Vector2f dir;

		if (pressed(key::D) || pressed(key::Right))
			dir.x += 1;
		if (pressed(key::A) || pressed(key::Left))
			dir.x += -1;
		if (pressed(key::W) || pressed(key::Up))
			dir.y += 1;
		if (pressed(key::S) || pressed(key::Down))
			dir.y += -1;

		std::cout << dir.x << " " << dir.y << std::endl;

		return dir;
	}
}