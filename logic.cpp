#include "logic.h"

GameLogic::GameLogic() {
	GameOver = false;

	Draw::init();
}

GameLogic::~GameLogic() {  }

void GameLogic::loop() {
	while (!GameOver) {
		Input::getInputDirection();

		Draw::drawLogic();
	}
}