#pragma once
#include <string>
#include <SFML/Graphics.hpp>

class Person {
private:
	sf::CircleShape playerIcon;

public:

	int x;
	int y;
	std::string name;
	std::string id;

	Person();
	Person(std::string Name, int id, int x, int y);

	void SetInfo(int X, int Y);
	void draw();
	void moveX(int x);
	void moveY(int y);
	int getPosX();
	int getPosY();
	void setX(int x);
	void setY(int y);
};
