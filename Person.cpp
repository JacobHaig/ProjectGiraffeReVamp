#include "person.h"
#include "draw.h"
using namespace std;

Person::Person() {
	id = 1;
	name = "No Name";
	x = 15;
	y = 15;
}

Person::Person(string Name, int Id, int X, int Y) {
	id = Id;
	name = Name;
	x = X;
	y = Y;
}

void Person::draw() {
	playerIcon.setPosition(this->x, this->y);
	playerIcon.setFillColor(sf::Color::Red);
	playerIcon.setRadius(10.f);

	Draw::window->draw(playerIcon);
}

void Person::SetInfo(int x, int y) {
	this->x = x;
	this->y = y;
}
void Person::moveX(int x) {
	SetInfo(this->x + x, this->y);
}
void Person::moveY(int y) {
	SetInfo(this->x, this->y + y);
}
int Person::getPosX() {
	return this->x;
}
int Person::getPosY() {
	return this->y;
}

void Person::setX(int x) {
	this->x = x;
}
void Person::setY(int y) {
	this->y = y;
}