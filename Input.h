#pragma once

#include "SFML/Graphics.hpp"
#include "logic.h"

namespace Input {
	void getInput();
	sf::Vector2f getInputDirection();
};
