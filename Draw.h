#pragma once
#include <SFML/Graphics.hpp>
#include "input.h"
#include "logic.h"

#define WIDTH 800
#define HEIGHT 500

namespace Draw {
	static sf::RectangleShape background;
	static sf::RenderWindow* window;

	void init();
	void drawLogic();
	void drawEnemies();

	void drawMaps(int selection);
};